variable "public_subnet_cidrs" {
    type        = list(string)
    description = "Public Subnet CIDR values"
    default     = ["10.0.1.0/24"] #, "10.0.2.0/24" , "10.0.3.0/24"]
}
 
variable "private_subnet_cidrs" {
    type        = list(string)
    description = "Private Subnet CIDR values"
    default     = ["10.0.4.0/24"] #, "10.0.5.0/24" , "10.0.6.0/24"]
}

variable "azs" {
    type        = list(string)
    description = "Availability Zones"
    default     = ["us-east-1a", "us-east-1b", "us-east-1c"]
}

variable "aws_region" {
    description = "EC2 Region for the VPC"
    default = "us-east-1"
}

variable "vpc_cidr" {
    description = "CIDR for the whole VPC"
    default = "10.0.0.0/16"
}

variable "ami_nat" {
    description = "AMIs by region"
    default = {
        us-east-1 = "ami-0aa210fd2121a98b7"
    }
}

variable "amis" {
    description = "AMIs by region"
    default = {
        us-east-1 = "ami-0230bd60aa48260c6"
    }
}

# variable "aws_key_name" {}