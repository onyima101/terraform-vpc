# # store tfstate in s3 and locking information in DynamoDB
# terraform {
#   backend "s3" {
#     encrypt = true
#     # bucket = "${aws_s3_bucket.terraform-state-storage-s3.bucket}"
#     bucket = "nd-tf-state-s3"
#     # region = "${aws_s3_bucket.terraform-state-storage-s3.region}"
#     region = "us-east-1"
#     dynamodb_table = "nd-tf-state-lock-dynamo"
#     key = "terraform-state/terraform.tfstate"
#   }
# }

