# # terraform state file setup
# # create an S3 bucket to store the state file in
# resource "aws_s3_bucket" "terraform-state-storage-s3" {
#   bucket = "nd-tf-state-s3"

#   lifecycle {
#     prevent_destroy = false
#   }

#   tags = {
#     name = "S3 Remote Terraform State Store"
#     env = "dev"
#   }
# }

# resource "aws_s3_bucket_versioning" "terraform-state-storage-s3" {
#   bucket = aws_s3_bucket.terraform-state-storage-s3.id
#   versioning_configuration {
#     status = "Disabled"
#   }
# }  


# # create a DynamoDB table for locking the state file
# resource "aws_dynamodb_table" "dynamodb-terraform-state-lock" {
#   name = "nd-tf-state-lock-dynamo"
#   hash_key = "LockID"
#   read_capacity = 5
#   write_capacity = 5

#   attribute {
#     name = "LockID"
#     type = "S"
#   }

#   tags = {
#     name = "DynamoDB Terraform State Lock Table"
#     env = "dev"
#   }
# }
