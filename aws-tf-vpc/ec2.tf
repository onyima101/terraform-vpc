resource "aws_instance" "Bastion" {
  ami           = "${lookup(var.ami_nat, var.aws_region)}"  
  availability_zone = "${lookup(var.azs)}"
  instance_type = "t2.micro"
#   key_name = "${var.aws_key_name}"
  vpc_security_group_ids = ["${aws_security_group.App-SG.id}"]  
  subnet_id     = "${aws_subnet.public_subnet.id}"
  source_dest_check = false
  tags = {
    Name = "Bastion-Host"
  }
}

# This is for the Bastion Host to function as a NAT Instance
resource "aws_eip" "Bastion" {
    instance = "${aws_instance.Bastion.id}"
    vpc = true
}

resource "aws_instance" "private-instance" {
  ami           = "${lookup(var.amis, var.aws_region)}" 
  availability_zone = "${lookup(var.azs)}"  #"us-east-1a"
  instance_type = "t2.micro"
#   key_name = "${var.aws_key_name}"
  vpc_security_group_ids = ["${aws_security_group.App-SG.id}"]
  subnet_id = "${aws_subnet.public_subnet.id}"
  source_dest_check = false
  tags = {
    Name = "private-instance"
  }
}