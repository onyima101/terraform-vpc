# variable "aws_access_key" {}
# variable "aws_secret_key" {}
# variable "aws_key_path" {}
variable "aws_key_name" {}

variable "aws_region" {
    description = "EC2 Region for the VPC"
    default = "us-east-1"
}

variable "amis" {
    description = "AMIs by region"
    default = "ami-0fc5d935ebf8bc3bc" # Default ubuntu 22.04 LTS # For Amazon linux ami-0230bd60aa48260c6" # 
}

variable "amz_linux_ami" {
    description = "AMIs by region"
    default = "ami-0230bd60aa48260c6" # Default Amazon Linux # My Jenkins AMI."ami-0e269507131fe5202" #  
}

variable "vpc_cidr" {
    description = "CIDR for the whole VPC"
    default = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
    type        = list(string)
    description = "CIDR for the Public Subnet"
    default = ["10.0.0.0/24", "10.0.2.0/24", "10.0.4.0/24"]
}

variable "private_subnet_cidr" {
    type        = list(string)
    description = "CIDR for the Private Subnet"
    default = ["10.0.1.0/24", "10.0.3.0/24", "10.0.5.0/24"]
}

variable "azs" {
    type        = list(string)
    description = "Availability Zones"
    default     = ["us-east-1a", "us-east-1b", "us-east-1c"]
}
