/*
  Web Servers
*/
resource "aws_security_group" "web" {
    name = "vpc_web"
    description = "Allow incoming HTTP connections."

    # ingress {
    #     from_port = 22
    #     to_port = 22
    #     protocol = "tcp"
    #     cidr_blocks = ["${var.public_subnet_cidr}"]
    #     # security_groups = ["${aws_security_group.bastion_nat.id}"]
    # } 
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }     
    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }       
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress { 
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress { # SSH
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = var.private_subnet_cidr
    }    
    # egress { # SQL Server
    #     from_port = 1433
    #     to_port = 1433
    #     protocol = "tcp"
    #     cidr_blocks = ["${var.private_subnet_cidr}"]
    # }
    # egress { # MySQL
    #     from_port = 3306
    #     to_port = 3306
    #     protocol = "tcp"
    #     cidr_blocks = ["${var.private_subnet_cidr}"]
    # }

    vpc_id = "${aws_vpc.default.id}"

    tags = {
        Name = "WebServerSG"
    }
}

resource "aws_instance" "web-1" {
    ami =  "${var.amz_linux_ami}" 
    availability_zone = element(var.azs, count.index) #"${lookup(var.azs)}"
    instance_type = "t3.small"
    key_name = "${var.aws_key_name}"
    vpc_security_group_ids = ["${aws_security_group.web.id}"]
    subnet_id = aws_subnet.us-east-1a-public[0].id #"${aws_subnet.us-east-1a-public.id}"
    count = 1
    associate_public_ip_address = true
    source_dest_check = false

    tags = {
        Name = "Jenkins"
    }
}

resource "aws_eip" "web-1" {
    count = 1
    instance = aws_instance.web-1[count.index].id
}

# resource "aws_instance" "web-2" {
#     ami = #"${var.amis}" 
#     availability_zone = element(var.azs, count.index) #"us-east-1a"
#     instance_type = "t3.small"
#     key_name = "${var.aws_key_name}"
#     vpc_security_group_ids = ["${aws_security_group.web.id}"]
#     subnet_id = "${aws_subnet.us-east-1a-public.id}"
#     associate_public_ip_address = true
#     source_dest_check = false

#     tags = {
#         Name = "Jenkins-Node"
#     }
# }

# resource "aws_eip" "web-2" {
#     instance = "${aws_instance.web-2.id}"
# }