resource "aws_vpc" "default" {
    cidr_block = "${var.vpc_cidr}"
    enable_dns_support = true
    enable_dns_hostnames = true
    tags = {
        Name = "tf-aws-vpc"
    }
}

resource "aws_internet_gateway" "default" {
    vpc_id = "${aws_vpc.default.id}"
    tags = {
        Name = "tf-aws-vpc-IGW"
    }    
}

/*
  NAT Instance
*/
resource "aws_security_group" "bastion_nat" {
    name = "bastion_nat"
    description = "Allow traffic to pass from the private subnet to the internet"

    # ingress {
    #     description = "Ingress CIDR"
    #     from_port = 0
    #     to_port = 0
    #     protocol = "-1"
    #     cidr_blocks = ["${var.vpc_cidr}"]
    #     ipv6_cidr_blocks = []
    #     prefix_list_ids = []
    #     security_groups = []
    #     self = true  
    # }  
    # ingress {
    #     from_port = 80
    #     to_port = 80
    #     protocol = "tcp"
    #     cidr_blocks = ["${var.private_subnet_cidr}"]
    # }
    # ingress {
    #     from_port = 443
    #     to_port = 443
    #     protocol = "tcp"
    #     cidr_blocks = ["${var.private_subnet_cidr}"]
    # } 
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = var.private_subnet_cidr
    }

    # egress {
    #     from_port = 80
    #     to_port = 80
    #     protocol = "tcp"
    #     cidr_blocks = ["0.0.0.0/0"]
    # }
    # egress {
    #     from_port = 443
    #     to_port = 443
    #     protocol = "tcp"
    #     cidr_blocks = ["0.0.0.0/0"]
    # }
    egress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = var.private_subnet_cidr #["${var.private_subnet_cidr}"]
    }
    egress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = var.private_subnet_cidr #["${var.public_subnet_cidr}"]
    }    
    egress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    # egress {
    #     description = "Default egress"
    #     from_port = 0
    #     to_port = 0
    #     protocol = "-1"
    #     cidr_blocks = ["0.0.0.0/0"]
    #     ipv6_cidr_blocks = []
    #     prefix_list_ids = []
    #     security_groups = []
    #     self = true        
    # }
    vpc_id = "${aws_vpc.default.id}"

    tags = {
        Name = "NATSG"
    }
}

resource "aws_instance" "bastion_nat" {
    ami = "${var.amis}" # special ami preconfigured to do Amazon Linux NAT #"ami-0aa210fd2121a98b7"
    availability_zone = "${var.azs[0]}" #"${lookup(var.azs)}"
    instance_type = "t2.micro" #t3.nano
    count = 1
    key_name = "${var.aws_key_name}"
    # vpc_security_group_ids = ["${aws_security_group.bastion_nat.id}"]
    # subnet_id = "${aws_subnet.us-east-1a-public.id}"
    # associate_public_ip_address = true
    # source_dest_check = false
    network_interface {
      network_interface_id = "${aws_network_interface.network_interface.id}" #element(aws_network_interface.network_interface[*].id, count.index)
      device_index = 0
    }

    # For Ubuntu NAT instance AMI
    user_data = <<EOT
#!/bin/bash
sudo /usr/bin/apt update
sudo /usr/bin/apt install ifupdown
/bin/echo '#!/bin/bash
if [[ $(sudo /usr/sbin/iptables -t nat -L) != *"MASQUERADE"* ]]; then
  /bin/echo 1 > /proc/sys/net/ipv4/ip_forward
  /usr/sbin/iptables -t nat -A POSTROUTING -s ${var.private_subnet_cidr[0]} -j MASQUERADE
fi
' | sudo /usr/bin/tee /etc/network/if-pre-up.d/nat-setup
sudo chmod +x /etc/network/if-pre-up.d/nat-setup
sudo /etc/network/if-pre-up.d/nat-setup 
  EOT

    # For Linux AMI
    # user_data = <<-EOF
    #           #!/bin/bash
                # Enable Iptables
    #           sudo yum install iptables-services -y
    #           sudo systemctl enable iptables
    #           sudo systemctl start iptables
    #           # Your NAT instance setup script or configuration
    #           sysctl -w net.ipv4.ip_forward=1
    #           /sbin/iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
                # Other commands
    #           sudo /sbin/iptables -F FORWARD
    #           sudo service iptables save
    #           EOF    

    tags = {
        Name = "NAT-Bastion"
    }
}

resource "aws_eip" "nat" {
    instance = "${aws_instance.bastion_nat[0].id}"
    tags = {
        Name = "nat-eip"
    }
}

/*
  Public Subnet
*/
resource "aws_subnet" "us-east-1a-public" {
    count = length(var.public_subnet_cidr)
    vpc_id = "${aws_vpc.default.id}"
    map_public_ip_on_launch = true
    cidr_block = element(var.public_subnet_cidr, count.index) #"${var.public_subnet_cidr}"
    availability_zone = element(var.azs, count.index)

    tags = {
        Name = "Public Subnet ${count.index + 1}"
    }
}

resource "aws_route_table" "us-east-1a-public" {
    vpc_id = "${aws_vpc.default.id}"

    route {
        cidr_block = "0.0.0.0/0" #"${var.vpc_cidr}"
        gateway_id = "${aws_internet_gateway.default.id}"
    }

    tags = {
        Name = "Public-route-table"
    }
}

resource "aws_route_table_association" "us-east-1a-public" {
    count = length(var.public_subnet_cidr)
    subnet_id = element(aws_subnet.us-east-1a-public[*].id, count.index) 
    route_table_id = aws_route_table.us-east-1a-public.id
}

/*
  Private Subnet
*/
resource "aws_subnet" "us-east-1a-private" {
    count = length(var.private_subnet_cidr)
    vpc_id = "${aws_vpc.default.id}"
    cidr_block = element(var.private_subnet_cidr, count.index) 
    availability_zone = element(var.azs, count.index)

    tags = {
        Name = "Private Subnet ${count.index + 1}"
    }
}

# use this network interface for the private subnet route table route
resource "aws_network_interface" "network_interface" {
    subnet_id = aws_subnet.us-east-1a-public[0].id #"${aws_subnet.us-east-1a-public.id}"
    source_dest_check = false
    security_groups = [aws_security_group.bastion_nat.id]

    tags = {
    Name = "NAT_network_interface"
    }
}

resource "aws_route_table" "us-east-1a-private" {
    vpc_id = "${aws_vpc.default.id}"

    route {
        cidr_block = "0.0.0.0/0"
        network_interface_id = "${aws_network_interface.network_interface.id}"
    }

    tags = {
        Name = "Private-route-table"
    }
}

resource "aws_route_table_association" "us-east-1a-private-0" {
    count = 1
    subnet_id = aws_subnet.us-east-1a-private[0].id #element(aws_subnet.us-east-1a-private[*].id, count.index) 
    route_table_id = "${aws_route_table.us-east-1a-private.id}"
}

resource "aws_route_table_association" "us-east-1a-private-1" {
    count = 1
    subnet_id = aws_subnet.us-east-1a-private[1].id #element(aws_subnet.us-east-1a-private[*].id, count.index) 
    route_table_id = "${aws_route_table.us-east-1a-private.id}"
}

resource "aws_route_table_association" "us-east-1a-private-2" {
    count = 1
    subnet_id = aws_subnet.us-east-1a-private[2].id #element(aws_subnet.us-east-1a-private[*].id, count.index) 
    route_table_id = "${aws_route_table.us-east-1a-private.id}"
}

resource "aws_key_pair" "my_key_pair" {
#   key_name   = "${var.aws_key_name}"
  public_key = file("~/.aws/ndcc-key.pub")
}